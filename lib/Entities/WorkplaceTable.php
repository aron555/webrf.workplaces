<?php

namespace Webrf\Workplaces\Entities;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\SystemException;

class WorkplaceTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'webrf_workplaces';
    }

    /**
     * Returns entity map definition.
     * @return array
     * @throws ArgumentException
     * @throws SystemException
     */
    public static function getMap(): array
    {
        return [
            (new IntegerField('ID'))->configurePrimary()->configureAutocomplete()
            ,
            (new StringField('ADDRESS'))->configureRequired()
            ,
            (new IntegerField('USER_ID'))
            ,
            (new ReferenceField(
                'USER',
                'Bitrix\Main\UserTable',
                ['=this.USER_ID' => 'ref.ID']
            ))
            ,
            (new StringField('STATUS'))
        ];
    }
}