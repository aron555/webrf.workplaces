<?php

namespace Webrf\Workplaces\EventHandlers;

use Bitrix\Crm\DealTable;
use CPullStack;
use Webrf\Workplaces\Repositories\DealRepository;

class DealHandler
{

    /**
     * Обработка события "после добавления сделки"
     * @param array $arFields
     * @return void
     */
    public static function OnAfterCrmDealAdd(array $arFields): void
    {
        // Создана новая сделка
        if ($arFields['STAGE_ID'] === DealRepository::STAGE_NEW) {
            // Направляется уведомление пользователю
            CPullStack::AddByUser($arFields['ASSIGNED_BY_ID'], 'О необходимости выбрать рабочее место');

            /* Сделка переходит в статус «в работе» */
            (new DealRepository)->updateDeal(
                $arFields['ID'],
                [
                    'STAGE_ID' => DealRepository::STAGE_WORK,
                ]
            );
        }
    }

    /**
     * Обработка события "после изменения сделки"
     * @param array $arFields
     * @return void
     */
    public static function OnAfterCrmDealUpdate(array $arFields): void
    {
        /* Рабочее место выбрано */
        if ($arFields['STAGE_ID'] === DealRepository::STAGE_WORK) {


            (new DealRepository)->updateDeal(
                $arFields['ID'],
                [
                    'STAGE_ID' => DealRepository::STAGE_SUCCESS, // статус «выполнена»
                    'WORKPLACE_ID' => DealRepository::STAGE_WORK, // «рабочее место»
                ]
            );
        }
    }
}