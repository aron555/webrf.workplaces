<?php

declare(strict_types=1);

namespace Webrf\Workplaces\Repositories;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Webrf\Workplaces\Entities\WorkplaceTable;

class WorkplaceRepository
{
    public const STATUS_FREE = 'свободно';
    public const STATUS_BUSY = 'занято';

    /**
     * Логика освобождения рабочего места при деактивации пользователя
     * @param int $userId
     * @return void
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function releaseWorkplace(int $userId): void
    {
        // Получение ID занятого рабочего места по ID сотрудника
        $workPlaceObj = WorkplaceTable::getList(['filter' => ['USER_ID' => $userId]])->fetchObject();

        // Освобождение рабочего места и обновление статуса рабочего места
        WorkplaceTable::update(
            $workPlaceObj->getId(),
            [
                'USER_ID' => null,
                'STATUS' => self::STATUS_FREE,
            ]
        );

    }
}