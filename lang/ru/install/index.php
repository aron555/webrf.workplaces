<?php
$MESS['WEBRF_WORKPLACES_NAME'] = 'Модуль автоматизирующий закрепление и открепление рабочих мест сотрудников';
$MESS['WEBRF_WORKPLACES_DESCRIPTION'] = '';
$MESS['WEBRF_WORKPLACES_PARTNER_NAME'] = 'WEBRF';
$MESS['WEBRF_WORKPLACES_PARTNER_URI'] = '';
$MESS['WEBRF_WORKPLACES_INSTALL_ERROR_VERSION'] = 'Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.';
$MESS['WEBRF_WORKPLACES_INSTALL_TITLE'] = 'Установка модуля';
$MESS['WEBRF_WORKPLACES_UNINSTALL_TITLE'] = 'Деинсталляция модуля';