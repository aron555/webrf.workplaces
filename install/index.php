<?php

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\DB\SqlQueryException;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\SystemException;
use Webrf\Workplaces\Entities\WorkplaceTable;
use Webrf\Workplaces\EventHandlers\DealHandler;
use Webrf\Workplaces\EventHandlers\EmployeeHandlers;

Loc::loadMessages(__FILE__);

class webrf_workplaces extends CModule
{
    public function __construct()
    {
        if (file_exists(__DIR__ . '/version.php')) {
            $arModuleVersion = [];

            include_once(__DIR__ . '/version.php');

            $this->MODULE_ID = 'webrf.workplaces';
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
            $this->MODULE_NAME = Loc::getMessage('WEBRF_WORKPLACES_NAME');
            $this->MODULE_DESCRIPTION = Loc::getMessage('WEBRF_WORKPLACES_DESCRIPTION');
            $this->PARTNER_NAME = Loc::getMessage('WEBRF_WORKPLACES_PARTNER_NAME');
            $this->PARTNER_URI = Loc::getMessage('WEBRF_WORKPLACES_PARTNER_URI');
        }
    }

    /**
     * @return void
     * @throws ArgumentException
     * @throws LoaderException
     * @throws SystemException
     */
    public function DoInstall(): void
    {
        global $APPLICATION;

        if (version_compare(ModuleManager::getVersion('main'), '14.00.00')) {
            ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallDB();

            $this->InstallEvents();
        } else {
            $APPLICATION->ThrowException(
                Loc::getMessage('WEBRF_WORKPLACES_INSTALL_ERROR_VERSION')
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage('WEBRF_WORKPLACES_INSTALL_TITLE') . " \"" . Loc::getMessage('WEBRF_WORKPLACES_NAME') . "\"",
            __DIR__ . '/step.php'
        );
    }

    /**
     * @return void
     */
    public function InstallFiles(): void
    {
    }

    /**
     * @return void
     * @throws ArgumentException
     * @throws LoaderException
     * @throws SystemException
     */
    public function InstallDB(): void
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            // Создание таблицы для рабочих мест
            $instance = Base::getInstance(WorkplaceTable::class);
            $connection = $instance->getConnection();

            if (Base::isExists(WorkplaceTable::class) && !$connection->isTableExists(WorkplaceTable::getTableName())) {
                $instance->createDbTable();
            }
        }
    }

    /**
     * @return void
     */
    public function InstallEvents(): void
    {
        $eventManager = EventManager::getInstance();

        $eventManager->registerEventHandler(
            'main',
            'OnAfterUserAdd',
            $this->MODULE_ID,
            EmployeeHandlers::class,
            'onAfterUserAdd'
        );
        $eventManager->registerEventHandler(
            'main',
            'OnBeforeUserUpdate',
            $this->MODULE_ID,
            EmployeeHandlers::class,
            'onBeforeUserUpdate'
        );

        $eventManager->addEventHandler(
            'crm',
            'OnAfterCrmDealAdd',
            [DealHandler::class, 'OnAfterCrmDealAdd']
        );

        $eventManager->addEventHandler(
            'crm',
            'OnAfterCrmDealAdd',
            [DealHandler::class, 'OnAfterCrmDealUpdate']
        );
    }

    /**
     * @return void
     * @throws ArgumentException
     * @throws ArgumentNullException
     * @throws LoaderException
     * @throws SqlQueryException
     * @throws SystemException
     */
    public function DoUninstall(): void
    {
        global $APPLICATION;

        $step = (int)$_REQUEST['step'];

        if ($step < 2) {
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage('WEBRF_WORKPLACES_UNINSTALL_TITLE') . " \"" . Loc::getMessage('WEBRF_WORKPLACES_NAME') . "\"" . ' шаг ' . $step + 1,
                $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/do_uninstall1.php'
            );
        } else {
            $this->UnInstallDB(
                [
                    'savedata' => $_REQUEST['savedata'],
                ]
            );

            $this->UnInstallEvents();

            ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(
                Loc::getMessage('WEBRF_WORKPLACES_UNINSTALL_TITLE') . " \"" . Loc::getMessage('WEBRF_WORKPLACES_NAME') . "\"",
                __DIR__ . '/unstep.php'
            );
        }
    }

    /**
     * @return void
     */
    public function UnInstallFiles()
    {
    }

    /**
     * @param array $arParams
     * @return void
     * @throws ArgumentException
     * @throws ArgumentNullException
     * @throws LoaderException
     * @throws SqlQueryException
     * @throws SystemException
     */
    public function UnInstallDB(array $arParams = []): void
    {
        if (array_key_exists('savedata', $arParams) && $arParams['savedata'] !== 'Y') {
            Option::delete($this->MODULE_ID);

            if (Loader::includeModule($this->MODULE_ID)) {
                // Удаление таблицы для рабочих мест
                if (Base::isExists(WorkplaceTable::class)) {
                    $connection = Base::getInstance(WorkplaceTable::class)->getConnection();

                    if ($connection->isTableExists(WorkplaceTable::getTableName())) {
                        $connection->dropTable(WorkplaceTable::getTableName());
                    }
                }
            }
        }
    }

    /**
     * @return void
     */
    public function UnInstallEvents(): void
    {
        $eventManager = EventManager::getInstance();

        $eventManager->unRegisterEventHandler(
            'main',
            'OnAfterUserAdd',
            $this->MODULE_ID,
            EmployeeHandlers::class,
            'onAfterUserAdd'
        );
        $eventManager->unRegisterEventHandler(
            'main',
            'OnBeforeUserUpdate',
            $this->MODULE_ID,
            EmployeeHandlers::class,
            'onBeforeUserUpdate'
        );
    }
}