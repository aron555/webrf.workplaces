<?php

namespace Webrf\Workplaces\Entities;

use Bitrix\Crm\DealTable;

class WorkPlaceDealTable extends DealTable
{
    public const CATEGORY_ID = 1;

    /**
     * @param $query
     * @return void
     */
    public static function setDefaultScope($query): void
    {
        $query->where('CATEGORY_ID', self::CATEGORY_ID);
    }
}