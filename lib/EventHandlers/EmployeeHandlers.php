<?php

declare(strict_types=1);

namespace Webrf\Workplaces\EventHandlers;


use Bitrix\Crm\DealTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use CGroup;
use Exception;
use Webrf\Workplaces\Repositories\DealRepository;
use Webrf\Workplaces\Repositories\WorkplaceRepository;

if (!Loader::includeModule('crm')) {
    throw new Exception('Module CRM no found');
}

class EmployeeHandlers
{

    public const WORK_GROUP_NAME = 'Работник';

    /**
     * @param array $arFields
     * @return void
     */
    public static function onAfterUserAdd(array $arFields): void
    {
        $rsGroups = CGroup::GetList('c_sort', 'desc', ['NAME' => self::WORK_GROUP_NAME]); // выбираем группы

        $findGroup = $rsGroups->fetch();

        // Проверка наличия группы "Работник" в массиве
        $inGroup = false;
        foreach ($arFields['GROUP_ID'] as $group) {
            if ($group['GROUP_ID'] == $findGroup['ID']) {
                $inGroup = true;
                break;
            }
        }

        if ($inGroup) {
            (new DealRepository)->createDeal($arFields['ID']);
        }

    }

    /**
     * @param array $arFields
     * @return void
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function onBeforeUserUpdate(array $arFields): void
    {
        // Логика освобождения рабочего места при деактивации пользователя
        if ($arFields['ACTIVE'] === 'N') {
            // Освобождение связанного рабочего места
            WorkplaceRepository::releaseWorkplace($arFields['ID']);
        }
    }
}