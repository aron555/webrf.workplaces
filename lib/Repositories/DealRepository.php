<?php

declare(strict_types=1);

namespace Webrf\Workplaces\Repositories;


use Webrf\Workplaces\Entities\WorkPlaceDealTable;

class DealRepository
{
    public const STAGE_NEW = 'IS_NEW';
    public const STAGE_WORK = 'IN_WORK';
    public const STAGE_SUCCESS = 'IS_SUCCESS';

    /**
     * Создаем сделку
     * @param int $userId
     * @return void
     */
    public function createDeal(int $userId): void
    {
        $dealFields = [
            'TITLE' => 'Новая сделка с работником ID ' . $userId,
            'ASSIGNED_BY_ID' => $userId,
            'STAGE_ID' => self::STAGE_NEW, // стадия
            'OPENED' => 'Y',
        ];

        $entity = new WorkPlaceDealTable();
        $res = $entity->add($dealFields);

        if ($res->isSucces()) {
            \Bitrix\Main\Diag\Debug::writeToFile($res->getId());
        }
    }

    public function updateDeal(int $id, array $fields): void
    {
        $entity = new WorkPlaceDealTable();
        $res = $entity->update($id, $fields);

        if ($res->isSucces()) {
            \Bitrix\Main\Diag\Debug::writeToFile($res->getId());
        }
    }
}